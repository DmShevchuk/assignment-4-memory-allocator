//
// Created by dmitr on 12.01.2023.
//
#include "test.h"
#include "mem.h"

void run_tests() {
    // Тест 1: Инициализация кучи
    void *heap = heap_init(10000);
    debug_heap(stdout, heap);
    printf("\n\n");

    // Тест 2: Нормальная аллокация
    void *first_block = _malloc(100);
    void *second_block = _malloc(300);
    _malloc(100);
    void *third_block = _malloc(1);
    _malloc(1000);
    debug_heap(stdout, heap);
    printf("\n\n");


    test3(third_block, heap);

    test4(first_block, second_block, heap);

    test5(heap);
}

// Тест 3: Освобождение одного блока
void test3(void *block, void *heap){
    _free(block);
    debug_heap(stdout, heap);
}

// Тест 4: Освобождение двух блоков
void test4(void *first_block, void *second_block, void *heap){
    _free(second_block);
    _free(first_block);
    debug_heap(stdout, heap);
}

// Тест 5: Случай, когда память закончилась и новый регион расширяет старый
void test5(void *heap){
    _malloc(300);
    _malloc(10000);
    _malloc(10500);
    _malloc(678);
    debug_heap(stdout, heap);
}