//
// Created by dmitr on 12.01.2023.
//

#ifndef ASSIGNMENT_4_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_4_MEMORY_ALLOCATOR_TEST_H

void run_tests();

void test3(void *block, void *heap);
void test4(void *first_block, void *second_block, void *heap);
void test5(void *heap);

#endif //ASSIGNMENT_4_MEMORY_ALLOCATOR_TEST_H
