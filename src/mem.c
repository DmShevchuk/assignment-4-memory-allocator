#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    if (!addr) {
        return REGION_INVALID;
    }


    size_t reg_actual_size = region_actual_size(query + offsetof(struct block_header, contents));

    struct region region = {.addr = map_pages(addr, reg_actual_size, MAP_FIXED_NOREPLACE),
            .extends = true,
            .size = reg_actual_size};

    if (region.addr == MAP_FAILED || region_is_invalid(&region)) {
        region.addr = map_pages(addr, reg_actual_size, 0);
        region.extends = false;

        if (region.addr == MAP_FAILED || region_is_invalid(&region)) {
            return REGION_INVALID;
        }
    }

    block_init(region.addr,
               (block_size) {.bytes = region.size},
               NULL);

    return region;

}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    
    if (region_is_invalid(&region)) {
        return NULL;
    }
    
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query 
    + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (!block_splittable(block, query)) return false;

    void *second_block_addr = block->contents + query;

    block_size new_block_size = (block_size) {.bytes = block->capacity.bytes - query};

    block_init(second_block_addr, new_block_size, block->next);

    block->next = second_block_addr;
    block->capacity.bytes = query;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    struct block_header *next_block = block->next;

    if (next_block == NULL || !mergeable(block, next_block)) return false;

    block->next = next_block->next;
    block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

struct block_search_result block_found(struct block_header* block) { 
  return (struct block_search_result) {
      .type = BSR_FOUND_GOOD_BLOCK,
      .block = block
    };
}

struct block_search_result block_not_found(struct block_header* block) { 
  return (struct block_search_result) {
      .type = BSR_REACHED_END_NOT_FOUND,
      .block = block
    };
}


static bool try_merge_with_next_to_end(struct block_header *block) {
    bool checker = false;
    while (block->next) {
        if (try_merge_with_next(block)) {
            checker = true;
        } else break;
    }
    return checker;
}


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header *found = block;
    struct block_header *current = block;

    while (current != NULL) {
        while (current->is_free && try_merge_with_next(current));

        if (current->is_free && block_is_big_enough(sz, current)) {
            return block_found(current);
        }

        found = current;
        current = current->next;

    }

    return block_not_found(found);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    size_t max_size = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = find_good_or_last(block, max_size);

    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, max_size);
        search_result.block->is_free = false;
    }

    return search_result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    
    if (!last) {
        return NULL;
    }

    const struct region new_region = alloc_region(block_after(last), query);

    last->next = new_region.addr;

    if (new_region.extends && last->is_free) {
        try_merge_with_next(last);
        return last;
    }

    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {

    struct block_search_result search_result = try_memalloc_existing(query, heap_start);
    struct block_header *last_block = search_result.block;

    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    }

    last_block = grow_heap(last_block, query);

    if (last_block == NULL) {
        return last_block;
    }

    return try_memalloc_existing(query, last_block).block;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
            struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    
    try_merge_with_next_to_end(header);
}
